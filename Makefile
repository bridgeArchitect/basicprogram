COMPILER := as
COMPFLAGS := -gdwarf2

LINKER := ld
LDFLAGS := -static

all: exit.o
	$(LINKER) $(LDFLAGS) -o exit exit.o

exit: exit.o
	$(LINKER) $(LDFLAGS) -o exit exit.o

exit.o: exit.s
	$(COMPILER) $(COMPFLAGS) -o exit.o -c exit.s
	
clean:
	rm *.o; rm exit